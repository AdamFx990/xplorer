--[[
    NAME: fs.lua
    DESC: File system functions

    Functions in this file should not be called by love.draw() 
    where it can possibly be avoided!
]]

require "classes.Node"
require "classes.DesktopEntry"


-- Returns a table of nodes for the given directory
function scanDirectory(dir)
    local handle = io.popen('ls -1 '..dir)
    local lines = handle:lines()
    
    local nodes = {}
    -- Create a table of nodes
    for line in lines do
        -- Initalise a node
        local node = Node(dir,line)
        -- Add the node to table of nodes
        table.insert(nodes,node)
    end
    
    handle:close()
    
    return nodes
end -- scanDirectory()


function getWorkingDir()
    local handle = io.popen('echo $PWD')
    local result = getOutputOf('echo $PWD')
    
    handle:close()
    -- remove new lines
    result = string.gsub(result, "\n", "")
    
    return result
end -- getWorkingDir()


function getDirUpALevel(dir)
    local length = string.len(dir)
    
    if length < 1 then
        return '/'
    end
    
    local result = string.reverse(dir)
    -- find the index of the first /
    local i = string.find(result, '/')
    -- trim everything before the slash
    result = string.sub(result, (i + 1), len)
    result = string.reverse(result)

    return result
end -- getDirUpALevel()


-- returns a string with the output of a command
function getOutputOf(cmd)
    local handle = io.popen(cmd)
    local result = handle:read()
    
    handle:close()

    removeNewLines(result)
    
    return result
end -- getOutputOf()


function removeNewLines(str)
    if result == nil then
        return ""
    else
        return string.gsub(result, "\n", "")
    end
end -- removeNewLines()


-- get the file extension from file name or full path
function getFileExtension(file)
    local i = lastInString(file)
    
    if i == -1 then
        return ""
    end

    local length = string.len(file)
    local extension = string.sub(file, i, length)

    return extension
end -- getFileExtension()


-- returns the index of the last occurance of a character in a string
function lastInString(str)
    local length = string.len(str)
    
    if length < 1 then
        return -1
    end

    str = string.reverse(str)

    local i = string.find(str, '%.')
    if i == nil then
        return -1
    end

    return (length - i) + 2
end -- lastInString()


-- handles launching different file types
function launchFile(node)
    local ext = node.extension
    
    if ext == "sh" then 
        os.execute(node.path)
    elseif ext == "desktop" then
        print("desktop entry found at: "..node.path)
        local desktop = DesktopEntry(node.path)
        desktop:run()
    end
end -- launchFile()


function fileExists(path)
    local file = io.open(path, "rb")
    if file then
        file:close()
        return true
    end
    return file ~= nil
end -- fileExists()


function linesFromFile(path)
    if not fileExists(path) then
        return {}
    end
    
    local lines = {}

    for line in io.lines(path) do
        lines[#lines + 1] = line
    end

    return lines
end -- lineFromFile()
