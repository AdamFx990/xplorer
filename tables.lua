-- tables.lua
-- Functions to help use and manipulate tables


-- returns the number of elements in a table (int)
function elementsInTable(table)
    local count = 0
    
    for key,val in ipairs(table) do
        count = count + 1
    end

    return count
end