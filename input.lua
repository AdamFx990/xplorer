-- input.lua
-- love input handling functions

require "graphics"


function love.gamepadpressed(joystick, button)
    if button == 'dpup' then
        upPressed()
    elseif button == 'dpdown' then
        downPressed()
    elseif button == 'b' then
        enterPressed()
    elseif button == 'a' then
        backPressed()
    elseif button == 'x' then
        homePressed()
    elseif button == 'y' then
        appShortcutPressed()
    end

    buttonPressed()
end -- love.gamepadpressed()


function love.keypressed(key, scancode, isRepeat)
    if key == 'up' then
        upPressed()
    elseif key == 'down' then
        downPressed()
    elseif key == 'return' then -- enter is called return (bloody yanks)
        enterPressed()
    elseif key == 'backspace' then
        backPressed()
    elseif key == 'h' then
        homePressed()
    elseif key == 'a' then
        appShortcutPressed()
    end

    buttonPressed()
end -- love.keypressed()


function love.mousepressed(x, y, button)
    print('\nMouse clicked at x:'..x..' y: '..y..' button: '..button)
end -- love.mousepressed()



-- Handle updates on button presses rather than every frame.
-- This will help us not destory SSDs!
function buttonPressed()
    nodes = scanDirectory(pwd)
end -- buttonPressed()


function upPressed()
    activeButton = activeButton - 1
    if activeButton < 1 then
        activeButton = elementsInTable(nodes)
    end
end -- upPressed()


function downPressed()
    activeButton = activeButton + 1
    if activeButton > elementsInTable(nodes) then
        activeButton = 1
    end
end -- downPressed()


function enterPressed()
    local node = nodes[activeButton]

    if node == nil then
        return
    end

    if node.type == "directory" then
        pwd = node.path
        activeButton = 1
    elseif node.type == "file" then
        launchFile(node)
    end
end -- enterPressed()


function backPressed()
    activeButton = 1
    pwd = getDirUpALevel(pwd)
end -- backPressed()


function homePressed()
    activeButton = 1
    pwd = '$HOME'
end -- homePressed()

function appShortcutPressed()
    activeButton = 1
    pwd = '/usr/share/applications/'
end -- appShortcutPressed()
