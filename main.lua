-- main.lua

require "classes.Button"
require "classes.Node"
require "fs" -- file system
require "graphics"
require "input"
require "tables"


function love.load()
    activeButton = 1

    pwd = getWorkingDir()
    nodes = scanDirectory(pwd)
end -- love.load()


function love.update()

end -- love.update()


function love.draw()
    drawButtons()
end -- love.draw()
