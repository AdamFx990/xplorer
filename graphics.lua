--[[
    NAME: graphics.lua
    DESC: Logic and vars for cAdalculating how/where to draw things.
]]--


smallFont = love.graphics.newFont(12)
largeFont = love.graphics.newFont(24)


-- draw buttons without running any fs functions
function drawButtons()
    local buttons = {}
    local width, height = love.graphics.getDimensions()

    love.graphics.setFont(largeFont)

    for key,val in ipairs(nodes) do
        local yOffset = (key - (activeButton + 1))
        local x = 16
        local y = ((48 * yOffset) + (height / 2))
        
        local active = false
        if key == activeButton then
            active = true
        end
        
        buttons[key] = Button(x, y, active, nodes[key].name)
    end

    buttons = nil
end -- drawButtons()