function love.conf(t)
    t.window.title = "xplorer"
    t.window.width = 800
    t.window.height = 600
    t.window.display = 3
    t.window.resizeable = true
end