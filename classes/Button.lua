-- Button.lua
-- A class for a drawing and handling a button

require "classes.Class" -- base class


Button = newClass(function(button, posx, posy, active, text)
    button.x = posx
    button.y = posy
    button.text = text
    button.active = active
    
    if active == true then
        love.graphics.setColor(200, 50, 50, 255)
    else
        love.graphics.setColor(0, 100, 200, 255)
    end

    love.graphics.rectangle("fill", posx, posy, 420, 48)

    love.graphics.setColor(0, 0, 0, 255)
    love.graphics.print(text, (posx + 16), (posy + 8))
end) -- Button()


return Button