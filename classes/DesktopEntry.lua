-- DesktopEntry.lua
-- A class representing a .desktop file

require "classes.Class" -- base class
require "parsers"


DesktopEntry = newClass(function(desktopEntry, file)
    local parsedFile = parseDesktopEntry(file)

    if parsedFile['name'] then
        print('name: '..parsedFile['name'])
        desktopEntry.name = parsedFile['name']
    end
    if parsedFile['exec'] then
        print('exec: '..parsedFile['exec'])
        desktopEntry.exec = parsedFile['exec']
    end
    if parsedFile['icon'] then
        print('icon: '..parsedFile['icon'])
        desktopEntry.icon = parsedFile['icon']
    end
end) -- DekstopEntry()


function DesktopEntry:run()
    os.execute(self.exec)
end -- run()


return DesktopEntry
