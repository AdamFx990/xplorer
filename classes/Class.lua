-- Class.lua
-- A table used as a basis for "class" tables
-- Requires Lua >= 5.1
-- Based on lua-users.org/wiki/SimpleLuaClasses
-- Look there for more detail on how this witchcraft works :)


function newClass(base, init)
    local Class = {}

    if not init and type(base) == 'function' then
        init = base
        base = nil
    elseif type(base) == 'table' then
        -- Derived class is a shallow copy of this class
        for i, v in pairs(base) do
            Class[i] = v
        end

        Class._base = base
    end
    -- The class will be the metatable for all its objects
    Class.__index = Class

    -- a generic constructor which can be called by <classname>(args)
    local mt = {} -- initalise the metatable
    
    mt.__call = function(class_tbl, ...)
    
        local obj = {}
        
        setmetatable(obj, Class)
        if init then
            init(obj, ...)
        else
            -- initialise everything in the base class
            if base and base.init then
                base.init(obj, ...)
            end
        end

        return obj
    end

    Class.init = init
    Class.is_a = function(self, cl) -- cl = class in Class.is_a
    local m = getmetatable(self) -- m = metatable
    
    -- search the metatable for the class
    while m do
        if m == cl then
            return true
        end
    end
        return false
    end
    
    setmetatable(Class, mt)
    
    return Class
end -- newClass()
