-- Node.lua
-- A class representing a filesystem node

require "classes.Class" -- base class


Node = newClass(function(node, parentDir, name)
    --print("node initalised with (path:"..path..", name: "..name..")")
    local extension = getFileExtension(string.lower(name))
    local path = parentDir..'/'..name
    local type = getOutputOf('sh scripts/nodeType.sh '..path)
    
    node.extension = extension
    node.parentDir = parentDir
    node.path = path
    node.type = type

    if type == "file" and extension == "desktop" then
        local desktop = parseDesktopEntry(path)
        node.name = desktop['name']
    else
        node.name = name
    end
end) -- Node()


return Node