-- parsers.lua
-- Functions for parsing files for useful information


function parseDesktopEntry(path)
    print("parsing file:"..path)
    local lines = linesFromFile(path)

    local result = {}
    local entry = false
    
    local line = 0

    for key, val in ipairs(lines) do
        line = line + 1
        print("    parsing line "..line..": "..val)

        -- ingore commented lines
        if not string.find(val, '%#') then
            --[[
            Some desktop entries contain 'Desktop Actions'. We want to ignore anything
            under these headings. Entry is true once we've passed [Desktop Entry] and
            false if we pass any other [Desktop ...] line.
            ]]--
            if string.find(val, '%[Desktop') then
                -- [Dekstop Entry]
                if string.find(val, 'Entry%]') then
                    entry = true
                else -- [Desktop ...]
                    entry = false
                end
            end
            
            -- [Desktop Entry]
            if entry then
                if string.find(val, 'Name') then
                    -- Use the default name
                    if string.find(val, '%[') == nil then
                        result['name'] = getStringAfterChar(val, '=')
                    end
                elseif string.find(val, 'Exec') then
                    result['exec'] = getStringAfterChar(val, '=')
                elseif string.find(val, 'Icon') and string.find(val, '%[') then
                    result['icon'] = getStringAfterChar(val, '=')
                end
            end

            -- once 3 lines have been found, break loop
            if table.getn(result) == 3 then
                break
            end
        end
        
    end

    return result
end -- parseDesktopEntry()


-- TODO: multimedia mimeType parser
function parseMimeTypes()
    local dir = '/usr/share/mime/'
    
end -- parseMimeTypes()


function getStringAfterChar(str, char)
    if not str or not char then
        return nil
    end

    local len = string.len(str)
    -- find the index following the given character
    local i = (string.find(str,char) + 1)
    
    return string.sub(str, i, len)
end -- getStringAfterChar()